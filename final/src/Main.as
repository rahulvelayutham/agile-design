package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author rahul_velayutham
	 */
	public class Main extends Sprite
	{
		private var cells:Array;
		public var n:int = 2;
		public var count:int = 0;
		public var vals:Array;
		
		
		public function Main():void
		{
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			trace(2==2==2)
			//get confirmation from both players
			var a:greetingpage = new greetingpage();
			var u:Array = new Array 
			u = a.startconfirm();
			trace("x="+u[0] ,"y="+u[1]);
			
			if (u[0] && u[1])
			{
				
				//draw board
				trace("adding board");
				[Embed(source="../lib/board.png")]
				var boardImageClass:Class;
				var boardImage:Sprite = new Sprite();
				//next line added mousecontrol
				boardImage.addEventListener(MouseEvent.CLICK, mouseClicked);
				boardImage.addChild(new boardImageClass());
				stage.addChild(boardImage);
			
				//create the cells for board
				cells = new Array();
				vals= new Array();
				for (var x:int = 0; x < 3; x++)
				{
					cells.push(new Array());
					vals.push(new Array());
					for (var y:int = 0; y < 3; y++)
					{
						cells[x].push(new CellView(x, y));
						vals[x].push(0);
						stage.addChild(cells[x][y]);
					}
				}
				popals(vals);
				[Embed(source="../lib/player1.png")]
				var player1Class:Class;
				var player1:Sprite = new Sprite();
			
				player1.addChild(new player1Class());
				player1.x = 0;
				player1.y = 400;
				stage.addChild(player1);
			}
			
			
			
		
		}
		
		private function popals(a:Array):void
		{
			a[0][0] = 2;
			a[0][1] = 3;
			a[0][2] = 4;
			a[1][0] = 5;
			a[1][1] = 6;
			a[1][2] = 7;
			a[2][0] = 8;
			a[2][1] = 9;
			a[2][2] = 10;
		}
		
		private function mouseClicked(e:MouseEvent):void
		{
			trace(n % 2,n)
			//trace(count)
			trace(vals[1][1])
			
			
			count = count + 1;
			if (count < 9)
			{	
				if((n%2)==0)
				{	
					[Embed(source="../lib/player2.png")]
					var player2Class:Class;
					var player2:Sprite = new Sprite();
			
					player2.addChild(new player2Class());
					player2.x = 0;
					player2.y = 400;
					stage.addChild(player2);
				
					n = n + 1;
					var cellX:int = e.localX / 100;
					var cellY:int = e.localY / 100;
					vals[cellX][cellY]=1;
					trace("Clicked at " + cellX + "," + cellY);
					cells[cellX][cellY].setSymbol(CellView.Cross);
					var t:int = 0;
					if (count > 4)
					{
							var h:check = new check()
							t = h.checking(vals);
							//trace(t)
							if (t==1)
							{
								endgamewin1();
							}
					}
				
				}
				else
				{	
					
					[Embed(source="../lib/player1.png")]
					var player1Class:Class;
					var player1:Sprite = new Sprite();
			
					player1.addChild(new player1Class());
					player1.x = 0;
					player1.y = 400;
					stage.addChild(player1);
				
					n = n + 1;
					var cellX:int = e.localX / 100;
					var cellY:int = e.localY / 100;
					vals[cellX][cellY]=0;
					trace("Clicked at " + cellX + "," + cellY);
					cells[cellX][cellY].setSymbol(CellView.Nought);
					var l:int = 0;
					if (count > 4)
					{
							var g:check = new check()
							l = g.checking(vals);
							//trace(l);
							trace(l);
							//trace(1==(l))
							if (l==1)
							{	trace("camehere");
								endgamewin2();
							}
					}
					//trace(vals[1][0],vals[1][1],vals[1][2])
					trace("in two");
				}
			}
			else 
			{	
					
					//trace(vals[1][0]);
					//trace(vals[1][1]);
					trace(vals[1][2]);
					
					[Embed(source="../lib/gameover.png")]
					var goClass:Class;
					var go:Sprite = new Sprite();
			
					go.addChild(new goClass());
				
					stage.addChild(go);
			}	
		}
				
		private function endgamewin1():void
		{			//trace(a)
					
					
						[Embed(source="../lib/win1.png")]
						var win1Class:Class;
						var win1:Sprite = new Sprite();
			
						win1.addChild(new win1Class());
				
						stage.addChild(win1);
					
		}
		
		private function endgamewin2():void
		{			//trace(a)
					
						[Embed(source="../lib/win2.png")]
						var win2Class:Class;
						var win2:Sprite = new Sprite();
			
						win2.addChild(new win2Class());
						stage.addChild(win2);
					
		}
		
		
	}

}