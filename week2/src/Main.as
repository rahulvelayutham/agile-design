package
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author rahul_velayutham
	 */
	public class Main extends Sprite
	{
		
		public function Main():void
		{
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			var val:Array = new Array();
			var a:drawbowl = new drawbowl();
			a.settrap();
			a.setpos();
			
			//test cases begin here
			a.testconst_leftex(-100);
			a.testconst_rightex(200);
			a.testconst_topex(100);
			a.testconst_botex(-200);
			val[0] = 50;
			val[1] = 50;
			a.testtrap(val);
		}
	}

}