package
{
	import flash.display.Sprite;
	/**
	 * ...
	 * @author rahul_velayutham
	 */
	
	 
	 //this class here deals with how the view looks !! ie clicking etc
	public class CellView extends Sprite
	{
		
		public static const Size:int = 100;
		
		public static const None:int = 0;
		public static const Cross:int = 1;
		public static const Nought:int = 2;
		
		private var cellX:int, cellY:int;
		private var symbol:int;
		
		private var currentImage:Sprite;
		
		[Embed(source="../lib/x.png")]
		private var crossClass:Class;
		[Embed(source="../lib/o.png")]
		private var noughtClass:Class;
		
		public function CellView(cellX:int, cellY:int)
		{
			symbol = None; // data initialization
			currentImage = null;
			
			this.cellX = cellX;
			this.cellY = cellY;
			
			x = cellX * Size + 5; // view initialization
			y = cellY * Size + 5;
		}
		
		public function setSymbol(symbol:int):void
		{
			this.symbol = symbol;
			Redraw();
		}
		
		public function Redraw():void
		{
			if (currentImage != null)
				removeChild(currentImage);
			
			switch (symbol)
			{
				case Nought: 
					currentImage = new Sprite();
					currentImage.addChild(new noughtClass());
					break;
				
				case Cross: 
					currentImage = new Sprite();
					currentImage.addChild(new crossClass());
					break;
				
				default: 
					currentImage = null;
					break;
			}
			if (currentImage != null)
				addChild(currentImage);
		}
	}
}