package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import asunit.textui.TestRunner;
    import flash.display.Sprite;
    import AllTests;
	
	/**
	 * ...
	 * @author rahul_velayutham
	 */
	public class Main extends Sprite 
	{
		
		public function Main():void 
		{
			if (stage) 
			{
				init();
			var unittests:TestRunner = new TestRunner();
			stage.addChild(unittests);
			unittests.start(AllTests, null, TestRunner.SHOW_TRACE);
			}
			else addEventListener(Event.ADDED_TO_STAGE, init);
			
			
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
	//		var a:drawHash = new drawHash(); //draw the tic tac toe board
		//	a.sel_diff();
		//	a.identify();
			
		}
		
	}
	
}