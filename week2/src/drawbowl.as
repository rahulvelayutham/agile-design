package
{
	
	/**
	 * ...
	 * @author rahul_velayutham
	 */
	public class drawbowl
	{
		var leftex:int = 0;
		var rightex:int = 0;
		var topex:int = 0;
		var botex:int = 0;
		var trapcoord:Array = new Array();
		var player1pos:int = 0;
		var player2pos:int = 0;
		var speed1:int = 0;
		var speed2:int = 0;
		
		//normal drawbowl functions
		public function drawbowl()
		{
			trace("constructor draw called");
			leftex = -100;
			rightex = 200;
			topex = 100;
			botex = -200;
			speed1 = 20;
			speed2 = 20;
		
		}
		
		public function settrap()
		{
			trapcoord[0] = (leftex + rightex) / 2;
			trapcoord[1] = (botex + topex) / 2;
			trace("the coords of the trap are", trapcoord[0], trapcoord[1]);
		}
		
		public function setpos()
		{
			player1pos = leftex + 50;
			player2pos = rightex - 50;
			trace("the position of the players are", player1pos, player2pos);
		}
		
		//test conditions are here
		public function testconst_leftex(val:int)
		{
			if (val == leftex)
			{
				trace("left extrmity initalised properly");
			}
			else
			{
				trace("left extremity was not initalised correctly");
			}
		}
		
		public function testconst_rightex(val:int)
		{
			if (val == rightex)
			{
				trace("right extrmity initalised properly");
			}
			else
			{
				trace("right extremity was not initalised correctly");
			}
		}
		
		public function testconst_topex(val:int)
		{
			if (val == topex)
			{
				trace("top extrmity initalised properly");
			}
			else
			{
				trace("top extremity was not initalised correctly");
			}
		}
		
		public function testconst_botex(val:int)
		{
			if (val == botex)
			{
				trace("bot extrmity initalised properly");
			}
			else
			{
				trace("bot extremity was not initalised correctly");
			}
		}
		
		public function testtrap(test:Array)
		{
			if (test[0] == trapcoord[0])
			{
				if (test[1] == trapcoord[1])
				{
					trace("the trap coords initalised correctly");
					
				}
				else
				{
					trace("y coord of trap not initalised right test case failed");
				}
			}
			else
			{
				trace("x coord of trap not initalised correctly");
			}
		}
	
	}

}